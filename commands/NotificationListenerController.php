<?php

namespace app\commands;

use app\Core\Application\Event\EventBus;
use app\Core\Infrastructure\Notification\ConnectionFabric;
use yii\console\Controller;
use function Zelenin\Hydrator\createObjectWithoutConstructor;
use Zelenin\Hydrator\NamingStrategy\RawNamingStrategy;
use Zelenin\Hydrator\Strategy\ReflectionStrategy;
use Zelenin\Hydrator\StrategyHydrator;

class NotificationListenerController extends Controller
{
    /**
     * @var EventBus
     */
    private $eventBus;

    /**
     * NotificationListenerController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param array $config
     * @param EventBus $eventBus
     */
    public function __construct($id, $module, $config = [], EventBus $eventBus)
    {
        $this->eventBus = $eventBus;
        parent::__construct($id, $module, $config);
    }

    public function actionRun()
    {
        $bindingKeys = ['app.core.application.event.#'];

        $connection = ConnectionFabric::createConnection();
        $channel = $connection->channel();
        $channel->exchange_declare('app_events', 'topic', false, false, false);
        list($queueName, ,) = $channel->queue_declare("", false, false, true, false);
        foreach($bindingKeys as $bindingKey) {
            $channel->queue_bind($queueName, 'app_events', $bindingKey);
        }
        echo ' [*] Waiting for events. To exit press CTRL+C', "\n";

        $callback = function($msg){
            echo " [x] Received ", $msg->body, "\n";
            $data = json_decode($msg->body, true);
           // try {
                $this->handleEvent($data['message']);
          //  } catch (\Exception $exception) {
                //\Yii::error('Notification queue: ' . $exception->getMessage() . ' ' . $msg->body, 'queue');
           // }
        };
        $channel->basic_consume($queueName, '', false, true, false, false, $callback);
        while(count($channel->callbacks)) {
            $channel->wait();
        }
        $channel->close();
        $connection->close();
    }

    /**
     * @param array $event
     */
    private function handleEvent(array $event)
    {
        $class = $event['class'];
        $payload = $event['payload'];
        $hydrator = new StrategyHydrator(new ReflectionStrategy(), new RawNamingStrategy());
        $event = createObjectWithoutConstructor($class);
        $event = $hydrator->hydrate($event, $payload);
        $this->eventBus->handle($event);
    }
}