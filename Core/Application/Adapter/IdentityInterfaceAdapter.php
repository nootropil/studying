<?php
declare(strict_types = 1);

namespace app\Core\Application\adapter;

use app\Core\Domain\Model\User\User;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use app\Core\Domain\Repository\User\UserReadRepository;
use yii\web\Session;

final class IdentityInterfaceAdapter implements IdentityInterface
{
    /**
     * @var UserReadRepository $userRepository
     */
    private $userRepository;

    /**
     * @var User|null
     */
    private $model;

    /**
     * IdentityInterfaceAdapter constructor.
     * @param $id
     * @param UserReadRepository $userRepository
     */
    public function __construct($id, UserReadRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        if($id == null) {
            $this->model = null;
        } else {
            $this->model = $this->userRepository->find($id);
        }
    }

    /**
     * @param string $id
     * @return IdentityInterfaceAdapter|null
     */
    public static function findIdentity($id) : ?IdentityInterfaceAdapter
    {
        /* @var $self IdentityInterfaceAdapter */
        $self = \Yii::createObject('app\Core\Application\adapter\IdentityInterfaceAdapter', [$id]);
        if ($self->getModel() !== null) {
            return $self;
        } else {
            (new Session())->destroy();
            return null;
        }
    }

    /**
     * @return User
     */
    public function getModel() : ?User
    {
        return $this->model;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->model->getId();
    }

    /**
     * @return string
     */
    public function getRole() : string
    {
        return $this->model->getRole();
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : \DateTime
    {
        return $this->model->getCreated();
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->model->getUsername();
    }

    /**
     * @return bool
     */
    public function isBlocked() : bool
    {
        return $this->model->isBlocked();
    }

    /**
     * @return bool
     */
    public function isAdmin() : bool
    {
        return $this->model->isAdmin();
    }

    /**
     * @return bool
     */
    public function isModerator() : bool
    {
        return $this->model->isModerator();
    }

    /**
     * @return bool
     */
    public function isUser() : bool
    {
        return $this->model->isUser();
    }

    /**
     * @return bool
     */
    public function isUnconfirmedUser() : bool
    {
        return $this->model->isUnconfirmedUser();
    }

    /**
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey) : bool
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param mixed $token
     * @param null $type
     * @throws NotSupportedException
     * @return null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException();
    }

    /**
     * @return string
     */
    public function getAuthKey() : ?string
    {
        return null;
    }
}