<?php
declare(strict_types = 1);

namespace app\Core\Application\Command;

use Zelenin\MessageBus\Context;

interface CommandBus
{
    /**
     * @param $command
     * @return Context
     */
    public function handle(Command $command) : Context;
}