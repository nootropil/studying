<?php
declare(strict_types = 1);

namespace app\Core\Application\Command;

use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Locator\MemoryLocator;
use Zelenin\MessageBus\MiddlewareBus\Middleware\HandlerMiddleware;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareBus;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareStack;

final class BaseCommandBus implements CommandBus
{
    /**
     * @param $command
     * @return Context
     */
    public function handle(Command $command) : Context
    {
        $className = get_class($command);
        $handlers = [
            $className => \Yii::createObject($className . 'Handler')
        ];
        $middlewares = [
            new HandlerMiddleware(new MemoryLocator($handlers))
        ];
        $commandBus = new MiddlewareBus(new MiddlewareStack($middlewares));
        return $commandBus->handle($command);
    }
}