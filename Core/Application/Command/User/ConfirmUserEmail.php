<?php
declare(strict_types = 1);

namespace app\Core\Application\Command\User;

use app\Core\Application\Command\Command;

final class ConfirmUserEmail implements Command
{
    /**
     * @var string
     */
    private $token;

    /**
     * ConfirmUserEmail constructor.
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

}