<?php
declare(strict_types = 1);

namespace app\Core\Application\Command\User;

use app\Core\Application\Command\Command;

final class UpdateUser implements Command
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $role;

    /**
     * @var string
     */
    private $status;


    /**
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->id = $attributes['id'];
        $this->username = $attributes['username'];
        $this->email = $attributes['email'];
        $this->role = $attributes['role'];
        $this->status = $attributes['status'];
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStatus() : string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getRole() : string
    {
        return $this->role;
    }
}