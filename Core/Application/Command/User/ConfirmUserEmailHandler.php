<?php
declare(strict_types = 1);

namespace app\Core\Application\Command\User;

use app\Core\Domain\Model\UserToken\Types;
use app\Core\Domain\Repository\User\UserReadRepository;
use app\Core\Domain\Repository\User\UserRepository;
use app\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use app\Core\Domain\Repository\UserToken\UserTokenRepository;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class ConfirmUserEmailHandler implements Handler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    /**
     * @var UserTokenReadRepository
     */
    private $userTokenReadRepository;

    /**
     * ConfirmUserEmailHandler constructor.
     * @param UserRepository $userRepository
     * @param UserReadRepository $userReadRepository
     * @param UserTokenReadRepository $userTokenReadRepository
     * @param UserTokenRepository $userTokenRepository
     */
    public function __construct(
        UserRepository $userRepository,
        UserReadRepository $userReadRepository,
        UserTokenReadRepository $userTokenReadRepository,
        UserTokenRepository $userTokenRepository
    )
    {
        $this->userTokenRepository = $userTokenRepository;
        $this->userTokenReadRepository = $userTokenReadRepository;
        $this->userRepository = $userRepository;
        $this->userReadRepository = $userReadRepository;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     */
    public function __invoke($command, Context $context): Context
    {
        $token = $this->userTokenReadRepository->findByTokenAndType($command->getToken(), Types::EMAIL_CONFIRMATION);
        $user = $this->userReadRepository->find($token->getUserId());
        $user->setRoleUser();
        $user->defineUpdated();
        $this->userRepository->save($user);
        $this->userTokenRepository->remove($token);
        return $context;
    }
}