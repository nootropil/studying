<?php
declare(strict_types = 1);

namespace app\Core\Application\Command\User;

use app\Core\Domain\Repository\User\UserReadRepository;
use app\Core\Domain\Repository\User\UserRepository;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class UpdateUserHandler implements Handler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * @param UserRepository $userRepository
     * @param UserReadRepository $userReadRepository
     */
    public function __construct(
        UserRepository $userRepository,
        UserReadRepository $userReadRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->userReadRepository = $userReadRepository;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     */
    public function __invoke($command, Context $context): Context
    {
        $user = $this->userReadRepository->find($command->getId());
        $user->changeUserData(
            $command->getUsername(),
            $command->getEmail(),
            $command->getRole()
        );
        $user->changeStatus($command->getStatus());
        $user->defineUpdated();
        $this->userRepository->save($user);
        return $context;
    }
}