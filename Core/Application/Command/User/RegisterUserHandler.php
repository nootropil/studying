<?php
declare(strict_types=1);

namespace app\Core\Application\Command\User;

use app\Core\Application\Event\AsyncEventBus;
use app\Core\Application\Event\User\UserRegistered;
use app\Core\Domain\Model\User\User;
use app\Core\Domain\Repository\User\UserRepository;
use app\Core\Domain\Service\User\PasswordHasher;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class RegisterUserHandler implements Handler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var PasswordHasher
     */
    private $passwordHasher;

    /**
     * @var AsyncEventBus
     */
    private $asyncEventBus;

    /**
     * RegisterUserHandler constructor.
     * @param UserRepository $userRepository
     * @param PasswordHasher $passwordHasher
     * @param AsyncEventBus $asyncEventBus
     */
    public function __construct(
        UserRepository $userRepository,
        PasswordHasher $passwordHasher,
        AsyncEventBus $asyncEventBus
    )
    {
        $this->userRepository = $userRepository;
        $this->passwordHasher = $passwordHasher;
        $this->asyncEventBus = $asyncEventBus;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     */
    public function __invoke($command, Context $context): Context
    {
        $id = $this->userRepository->nextIdentity();
        $user = User::registerNew(
            $id,
            $command->getUsername(),
            $command->getEmail(),
            $this->passwordHasher->hash($command->getPassword())
        );
        $this->userRepository->add($user);
        $this->asyncEventBus->handle(new UserRegistered($user->getId()));
        return $context;
    }
}