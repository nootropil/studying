<?php
declare(strict_types = 1);

namespace app\Core\Application\Command\User;

use app\Core\Domain\Model\UserToken\Types;
use app\Core\Domain\Repository\User\UserReadRepository;
use app\Core\Domain\Repository\User\UserRepository;
use app\Core\Domain\Service\User\PasswordHasher;
use app\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use app\Core\Domain\Repository\UserToken\UserTokenRepository;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;


final class RecoverUserPasswordHandler implements Handler
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var PasswordHasher
     */
    private $passwordHasher;

    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    /**
     * @var UserTokenReadRepository
     */
    private $userTokenReadRepository;

    /**
     * RecoverUserPasswordHandler constructor.
     * @param UserRepository $userRepository
     * @param PasswordHasher $passwordHasher
     * @param UserReadRepository $userReadRepository
     * @param UserTokenReadRepository $userTokenReadRepository
     * @param UserTokenRepository $userTokenRepository
     */
    public function __construct(
        UserRepository $userRepository,
        PasswordHasher $passwordHasher,
        UserReadRepository $userReadRepository,
        UserTokenReadRepository $userTokenReadRepository,
        UserTokenRepository $userTokenRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->passwordHasher = $passwordHasher;
        $this->userReadRepository = $userReadRepository;
        $this->userTokenRepository = $userTokenRepository;
        $this->userTokenReadRepository = $userTokenReadRepository;
    }

    /**
     * @param object $command
     * @param Context $context
     * @return Context
     */
    public function __invoke($command, Context $context): Context
    {
        $token = $this->userTokenReadRepository->findByTokenAndType($command->getToken(), Types::PASSWORD_RECOVERING);
        $user = $this->userReadRepository->find($token->getUserId());
        $user->changePasswordHash($this->passwordHasher->hash($command->getPassword()));
        $user->defineUpdated();
        $this->userRepository->save($user);
        $this->userTokenRepository->remove($token);
        return $context;
    }
}