<?php
declare(strict_types = 1);

namespace app\Core\Application\Command\User;

use app\Core\Application\Command\Command;
use app\Core\Domain\Model\User\User;

final class RegisterUser implements Command
{
    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->username = $attributes['username'];
        $this->email = $attributes['email'];
        $this->password = $attributes['password'];
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }
}