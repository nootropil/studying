<?php
declare(strict_types = 1);

namespace app\Core\Application\Command\User;

use app\Core\Application\Command\Command;

final class RecoverUserPassword implements Command
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $password;

    /**
     * RecoverUserPassword constructor.
     * @param string $token
     * @param string $password
     */
    public function __construct(string $token, string $password)
    {
        $this->token = $token;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getToken() : string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getPassword() : string
    {
        return $this->password;
    }

}