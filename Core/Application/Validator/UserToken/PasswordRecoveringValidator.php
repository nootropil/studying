<?php
declare(strict_types = 1);

namespace app\Core\Application\Validator\UserToken;

use app\Core\Domain\Model\UserToken\Types;
use app\Core\Domain\Repository\User\UserReadRepository;
use app\Core\Domain\Repository\UserToken\UserTokenReadRepository;

final class PasswordRecoveringValidator
{
    const EXPIRE_TOKEN = 3600;

    /**
     * @var UserReadRepository
     */
    private $userReadRepository;
    /**
     * @var UserTokenReadRepository
     */
    private $userTokenReadRepository;
    /**
     * @var bool
     */
    private $valid;
    /**
     * @var string
     */
    private $error;

    /**
     * EmailConfirmationValidator constructor.
     * @param UserReadRepository $userReadRepository
     * @param UserTokenReadRepository $userTokenReadRepository
     */
    public function __construct(
        UserReadRepository $userReadRepository,
        UserTokenReadRepository $userTokenReadRepository
    )
    {
        $this->userReadRepository = $userReadRepository;
        $this->userTokenReadRepository = $userTokenReadRepository;
    }

    public function validate(string $token) : self
    {
        if (strlen($token) < 1) {
            $this->valid = false;
            $this->error = 'Токен некорректен';
            return $this;
        }
        if(!$this->userTokenReadRepository->existsByTokenAndType($token, Types::PASSWORD_RECOVERING)) {
            $this->valid = false;
            $this->error = 'Токена не существует';
            return $this;
        }
        $userToken = $this->userTokenReadRepository->findByTokenAndType($token, Types::PASSWORD_RECOVERING);
        if ($userToken->getCreated()->getTimestamp() + self::EXPIRE_TOKEN  <= (new \DateTime('now'))->getTimestamp()) {
            $this->valid = false;
            $this->error = 'Токен просрочен';
        } else {
            $this->valid = true;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function isValid() : bool
    {
        return $this->valid;
    }

    /**
     * @return mixed
     */
    public function getError() : ?string
    {
        return $this->error;
    }
}