<?php
declare(strict_types = 1);

namespace app\Core\Application\Service\Notification\User;

use app\Core\Domain\Model\User\User;
use app\Core\Domain\Model\UserToken\UserToken;
use app\Core\Domain\Service\Notification\User\UserEmailNotificator;
use Yii;

class YiiUserEmailNotificator implements UserEmailNotificator
{
    /**
     * @param UserToken $token
     * @param User $user
     */
    public function sendEmailConformationEmail(UserToken $token, User $user) : void
    {
        \Yii::info('sendEmailConformationEmail', 'queue'); return; //TODO remove
        Yii::$app->mailer->compose(['html' => '@app/mail/templates/user/email_confirmation'], ['user' => $user, 'token' => $token])
            ->setFrom([Yii::$app->params['siteEmail'] => "test"])
            ->setTo($user->getEmail())
            ->setSubject('Registration')
            ->setCharset('utf-8')
            ->send();
    }

    /**
     * @param UserToken $token
     * @param User $user
     */
    public function sendPasswordRecoveringEmail(UserToken $token, User $user) : void
    {
        \Yii::info('sendPasswordRecoveringEmail', 'queue'); return; //TODO remove
        Yii::$app->mailer->compose(['html' => '@app/mail/templates/user/password_recovering'], ['user' => $user, 'token' => $token])
            ->setFrom([Yii::$app->params['siteEmail'] => "test"])
            ->setTo($user->getEmail())
            ->setSubject('Смена пароля')
            ->setCharset('utf-8')
            ->send();
    }
}