<?php
declare(strict_types = 1);

namespace app\Core\Application\Service\User;

use app\Core\Application\Adapter\IdentityInterfaceAdapter;
use app\Core\Domain\Repository\User\UserReadRepository;
use app\Core\Domain\Service\User\AuthService;
use app\Core\Infrastructure\Exception\CoreException;
use yii\base\Object;

final class YiiAuthService extends Object implements AuthService
{
    /**
     * @var UserReadRepository
     */
    private $userRepository;

    /**
     * @var string
     */
    private $errorMessage = 'Неправильный логин или пароль';

    /**
     * AuthService constructor.
     * @param UserReadRepository $userRepository
     * @param array $config
     */
    public function __construct(UserReadRepository $userRepository, $config = [])
    {
        $this->userRepository = $userRepository;
        parent::__construct($config);
    }

    /**
     * @param string $login
     * @param string $password
     * @return bool
     * @throws CoreException
     */
    public function login(string $login, string $password) : bool
    {
        $user = $this->userRepository->findByUsername($login);
        if($user !== null && $user->isBlocked()) {
            $this->errorMessage = 'Пользователь заблокирован';
            return false;
        }
        if ($user !== null && \Yii::$app->security->validatePassword($password, $user->getPasswordHash())) {
            $adapter = IdentityInterfaceAdapter::findIdentity($user->getId());
            return \Yii::$app->user->login($adapter, 3600 * 1);
        }
        return false;
    }

    /**
     * @return string
     */
    public function getErrorMessage() : string
    {
        return $this->errorMessage;
    }
} 