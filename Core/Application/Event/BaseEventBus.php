<?php
declare(strict_types=1);

namespace app\Core\Application\Event;

use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Locator\MemoryLocator;
use Zelenin\MessageBus\MiddlewareBus\Middleware\HandlerMiddleware;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareBus;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareStack;

final class BaseEventBus implements EventBus
{
    /**
     * @param $event
     * @return Context
     */
    public function handle(Event $event) : Context
    {
        $className = get_class($event);
        $handlers = [
            $className => \Yii::createObject($className . 'Handler')
        ];
        $middlewares = [
            new HandlerMiddleware((new MemoryLocator($handlers)))
        ];
        $eventBus = new MiddlewareBus(new MiddlewareStack($middlewares));
        return $eventBus->handle($event);
    }
}