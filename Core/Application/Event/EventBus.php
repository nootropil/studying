<?php
declare(strict_types = 1);

namespace app\Core\Application\Event;

use Zelenin\MessageBus\Context;

interface EventBus
{
    /**
     * @param $event
     * @return Context
     */
    public function handle(Event $event) : Context;
}