<?php
declare(strict_types=1);

namespace app\Core\Application\Event\Middleware;

use app\Core\Infrastructure\Notification\MessageQueue;
use app\Core\Infrastructure\Notification\RabbitMqMessage;
use Zelenin\Hydrator\NamingStrategy\RawNamingStrategy;
use Zelenin\Hydrator\Strategy\ReflectionStrategy;
use Zelenin\Hydrator\StrategyHydrator;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Locator\Locator;
use Zelenin\MessageBus\MiddlewareBus\Middleware;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareDispatcher;

final class RabbitMqMiddleware implements Middleware
{
    /**
     * @var Locator
     */
    private $locator;

    /**
     * @param Locator $locator
     */
    public function __construct(Locator $locator)
    {
        $this->locator = $locator;
    }

    /**
     * @inheritdoc
     */
    public function __invoke($message, MiddlewareDispatcher $dispatcher): Context
    {
        $messageQueue = \Yii::createObject(MessageQueue::class);
        $hydrator = new StrategyHydrator(new ReflectionStrategy(), new RawNamingStrategy());
        $data = $hydrator->extract($message);
        $className = get_class($message);
        $type = join('', array_slice(explode('\\', $className), -1));
        $rabbitMqMessage = new RabbitMqMessage(
            $this->normalizeClassName($className),
            $type,
            [
                'class' => $className,
                'payload' => $data
            ]
        );
        $messageQueue->send($rabbitMqMessage);
        return $dispatcher($message);
    }

    /**
     * @param string $className
     * @return string
     */
    private function normalizeClassName(string $className): string
    {
        $className = str_replace('\\', '.', $className);
        $pattern = ['#(?<=(?:\p{Lu}))(\p{Lu}\p{Ll})#', '#(?<=(?:\p{Ll}|\p{Nd}))(\p{Lu})#'];
        $replacement = ['_\1', '_\1'];
        return mb_strtolower(preg_replace($pattern, $replacement, $className));
    }
}