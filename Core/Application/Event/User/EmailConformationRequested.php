<?php
declare(strict_types = 1);

namespace app\Core\Application\Event\User;

use app\Core\Application\Event\Event;

final class EmailConformationRequested implements Event
{
    /**
     * @var string
     */
    private $userEmail;

    /**
     * EmailConformationRequested constructor.
     * @param string $userEmail
     */
    public function __construct(string $userEmail)
    {
        $this->userEmail = $userEmail;
    }

    /**
     * @return string
     */
    public function getUserEmail(): string
    {
        return $this->userEmail;
    }

}