<?php
declare(strict_types = 1);

namespace app\Core\Application\Event\User;

use app\Core\Domain\Model\UserToken\Types;
use app\Core\Domain\Model\UserToken\UserToken;
use app\Core\Domain\Repository\User\UserReadRepository;
use app\Core\Domain\Repository\UserToken\UserTokenRepository;
use app\Core\Domain\Service\Notification\User\UserEmailNotificator;
use app\Core\Domain\Service\String\RandomStringGenerator;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Handler;

final class UserRegisteredHandler implements Handler
{
    /**
     * @var UserReadRepository
     */
    private $userReadRepository;

    /**
     * @var UserTokenRepository
     */
    private $userTokenRepository;

    /**
     * @var RandomStringGenerator
     */
    private $randomStringGenerator;

    /**
     * @var UserEmailNotificator
     */
    private $userEmailNotificator;

    /**
     * EmailConformationRequestedHandler constructor.
     * @param UserReadRepository $userReadRepository
     * @param UserTokenRepository $userTokenRepository
     * @param RandomStringGenerator $randomStringGenerator
     * @param UserEmailNotificator $userEmailNotificator
     */
    public function __construct(
        UserReadRepository $userReadRepository,
        UserTokenRepository $userTokenRepository,
        RandomStringGenerator $randomStringGenerator,
        UserEmailNotificator $userEmailNotificator
    )
    {
        $this->userReadRepository = $userReadRepository;
        $this->userTokenRepository = $userTokenRepository;
        $this->randomStringGenerator = $randomStringGenerator;
        $this->userEmailNotificator = $userEmailNotificator;
    }

    /**
     * @param object $event
     * @param Context $context
     * @return Context
     */
    public function __invoke($event, Context $context): Context
    {
        $user = $this->userReadRepository->find($event->getUserId());
        $userTokenId = $this->userTokenRepository->nextIdentity();
        $userToken = UserToken::createNew(
            $userTokenId,
            $user->getId(),
            $this->randomStringGenerator->generate(64),
            Types::EMAIL_CONFIRMATION
        );
        $this->userTokenRepository->add($userToken);
        $this->userEmailNotificator->sendEmailConformationEmail($userToken, $user);
        return $context;
    }
}