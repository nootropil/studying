<?php
declare(strict_types = 1);

namespace app\Core\Application\Event\User;

use app\Core\Application\Event\Event;

final class UserRegistered implements Event
{
    /**
     * @var string
     */
    private $userId;

    /**
     * UserRegistered constructor.
     * @param string $userId
     */
    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }
}