<?php
declare(strict_types=1);

namespace app\Core\Application\Event;

use app\Core\Application\Event\Middleware\RabbitMqMiddleware;
use Zelenin\MessageBus\Context;
use Zelenin\MessageBus\Locator\MemoryLocator;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareBus;
use Zelenin\MessageBus\MiddlewareBus\MiddlewareStack;

final class RabbitMqEventBus implements AsyncEventBus
{
    /**
     * @param $event
     * @return Context
     */
    public function handle(Event $event) : Context
    {
        $className = get_class($event);
        $handlers = [
            $className => \Yii::createObject($className . 'Handler')
        ];
        $middlewares = [
            new RabbitMqMiddleware((new MemoryLocator($handlers)))
        ];
        $eventBus = new MiddlewareBus(new MiddlewareStack($middlewares));
        return $eventBus->handle($event);
    }
}