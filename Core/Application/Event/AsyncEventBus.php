<?php
declare(strict_types = 1);

namespace app\Core\Application\Event;


interface AsyncEventBus extends EventBus
{
}