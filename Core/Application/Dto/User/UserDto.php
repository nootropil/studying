<?php
declare(strict_types = 1);

namespace app\Core\Application\Dto\User;

use app\Core\Domain\Model\User\User;

final class UserDto
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $email;

    /**
     * @var int
     */
    public $role;

    /**
     * @var bool
     */
    public $status;

    /**
     * @var string
     */
    public $created;

    /**
     * @var string
     */
    public $updated;

    /**
     * UserViewDto constructor.
     * @param User $entity
     */
    public function __construct(User $entity)
    {
        $this->id = $entity->getId();
        $this->username = $entity->getUsername();
        $this->email = $entity->getEmail();
        $this->role = $entity->getRole();
        $this->status = $entity->getStatus();
        $this->created = $entity->getCreated();
        $this->updated = $entity->getUpdated();
    }
}