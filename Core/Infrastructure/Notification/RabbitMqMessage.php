<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Notification;

use JsonSerializable;

final class RabbitMqMessage implements JsonSerializable
{
    /**
     * @var string
     */
    private $topic;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array
     */
    private $message;

    /**
     * RabbitMqMessage constructor.
     * @param string $topic
     * @param string $type
     * @param array $message
     */
    public function __construct(string $topic, string $type, array $message)
    {
        $this->topic = $topic;
        $this->type = $type;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function getTopic(): string
    {
        return $this->topic;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }


    /**
     * @return string
     */
    public function jsonSerialize () : string
    {
        return json_encode(
            [
                'topic' => $this->topic,
                'type' => $this->type,
                'message' => $this->message
            ]
        );
    }
}
