<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Notification;

interface MessageQueue
{
    /**
     * @param RabbitMqMessage $event
     */
    public function send(RabbitMqMessage $event) : void;
}