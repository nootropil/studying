<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Notification;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

final class RabbitMessageQueue implements MessageQueue
{
    /**
     * @var AMQPStreamConnection
     */
    private $connection;

    /**
     * @return AMQPStreamConnection
     */
    private function getConnection() : AMQPStreamConnection
    {
        if(!($this->connection instanceof AMQPStreamConnection)) {
            $this->connection = ConnectionFabric::createConnection();
        }
        return $this->connection;
    }

    /**
     * @param RabbitMqMessage $message
     */
    public function send(RabbitMqMessage $message) : void
    {
        $channel = $this->getConnection()->channel();
        $channel->exchange_declare('app_events', 'topic', false, false, false);
        $msg = new AMQPMessage($message->jsonSerialize());
        $channel->basic_publish($msg, 'app_events', $message->getTopic());
        $channel->close();
    }

    public function __destruct()
    {
        if($this->connection instanceof AMQPStreamConnection) {
            $this->connection->close();
        }

    }
}
