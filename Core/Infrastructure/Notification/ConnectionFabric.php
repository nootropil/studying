<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Notification;

use PhpAmqpLib\Connection\AMQPStreamConnection;

final class ConnectionFabric
{
    private const USER = 'guest';
    private const PASSWORD = 'guest';
    private const HOST = 'localhost';
    private const PORT = 5672;

    /**
     * @return AMQPStreamConnection
     */
    public static function createConnection() : AMQPStreamConnection
    {
        return new AMQPStreamConnection(self::HOST, self::PORT, self::USER, self::PASSWORD);
    }
}
