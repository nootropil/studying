<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Service\File;

use app\Core\Infrastructure\Exception\CoreException;

final class FileStorage
{
    private const IMAGES_PATH = 'images';

    /**
     * @return string
     */
    public function generateFileName() : string
    {
        return time() .'_' . substr(md5(random_bytes(20)), 0, 20);
    }

    /**
     * @param string $file
     * @return string
     */
    public function generateCopyName(string $file) : string
    {
        return time() . '_' . 'copy_' . md5(random_bytes(15)) . '.'. (new \SplFileInfo($file))->getExtension();
    }

    /**
     * @return string
     */
    public function generateTempName() : string
    {
        return time() . '_' . 'temp_' . md5(random_bytes(15));
    }

    /**
     * @param string $file
     * @throws CoreException
     */
    private function deleteFile(string $file) : void
    {
        if($file === null) {
            throw new CoreException('Файл не передан');
        }
        if(!file_exists($file)) {
            throw new CoreException('Файл ' . $file .' не существует');
        }
        unlink($file);
    }

    /**
     * @param string $file
     * @return string
     */
    public static function setPathForImages(string $file) : string
    {
        return self::IMAGES_PATH . $file;
    }


}