<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Service\User;

use app\Core\Domain\Service\User\PasswordHasher;

final class YiiPasswordHasher implements PasswordHasher
{
    /**
     * @param string $password
     * @return string
     * @throws \yii\base\Exception
     */
    public function hash(string $password) : string
    {
        $hasher = new \yii\base\Security();
        return $hasher->generatePasswordHash($password);
    }
}