<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Service\String;

use app\Core\Domain\Service\String\RandomStringGenerator;

final class YiiRandomStringGenerator implements RandomStringGenerator
{
    /**
     * @param int $length
     * @return string
     */
    public function generate(?int $length) : string
    {
        return \Yii::$app->security->generateRandomString($length);
    }
}
