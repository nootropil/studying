<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Repository\UserToken;

use app\Core\Domain\Model\UserToken\UserToken;
use app\Core\Domain\Repository\UserToken\UserTokenRepository;
use app\Core\Infrastructure\Service\IdentityGenerator\Uuid4Generator;
use yii\db\Query;

final class YiiSqlUserTokenRepository implements UserTokenRepository
{
    const TABLE_NAME = 'user_token';

    /**
     * @var UserTokenHydrator
     */
    private $userTokenHydrator;

    /**
     * YiiSqlUserTokenReadRepository constructor.
     */
    public function __construct()
    {
        $this->userTokenHydrator = new UserTokenHydrator();
    }

    /**
     * @param UserToken $userToken
     */
    public function add(UserToken $userToken) : void
    {
        $columns = $this->userTokenHydrator->extract($userToken);
        $columns['id'] = $userToken->getId();
        $query = new Query();
        $query->createCommand()->insert(self::TABLE_NAME, $columns)->execute();
    }

    /**
     * @param UserToken $userToken
     */
    public function save(UserToken $userToken) : void
    {
        $columns = $this->userTokenHydrator->extract($userToken);
        $query = new Query();
        $query->createCommand()->update(self::TABLE_NAME, $columns, ['id' => $userToken->getId()])->execute();
    }

    /**
     * @param UserToken $userToken
     */
    public function remove(UserToken $userToken) : void
    {
        $query = new Query();
        $query->createCommand()->delete(self::TABLE_NAME, ['id' => $userToken->getId()])->execute();
    }

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity() : string
    {
        $uuidGenerator = new Uuid4Generator();
        return $uuidGenerator->generate();
    }  
}