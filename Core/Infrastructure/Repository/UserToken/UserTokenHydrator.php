<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Repository\UserToken;

use app\Core\Domain\Model\UserToken\UserToken;
use app\Core\Infrastructure\Repository\Hydrator;

final class UserTokenHydrator implements Hydrator
{
    /**
     * @param array $columns
     * @return UserToken
     */
    public function hydrate(array $columns) : UserToken
    {
        return UserToken::createFromStorage(
            $columns['id'],
            $columns['user_id'],
            $columns['token'],
            $columns['type'],
            $columns['created']
        );
    }

    /**
     * @param $entity
     * @return array
     */
    public function extract($entity) : array 
    {
        /* @var $entity UserToken */
        $columns = [
            'user_id' => $entity->getUserId(),
            'token' => $entity->getToken(),
            'type' => $entity->getType(),
            'created' => $entity->getCreated()->format('Y-m-d H:i:s')
        ];
        return $columns;
    }
}