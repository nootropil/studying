<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Repository\UserToken;

use app\Core\Domain\Model\UserToken\UserToken;
use app\Core\Domain\Repository\UserToken\UserTokenReadRepository;
use yii\db\Query;

final class YiiSqlUserTokenReadRepository implements UserTokenReadRepository
{
    /**
     * @var UserTokenHydrator
     */
    private $userTokenHydrator;

    /**
     * YiiSqlUserTokenReadRepository constructor.
     */
    public function __construct()
    {
        $this->userTokenHydrator = new UserTokenHydrator();
    }

    /**
     * @param $id
     * @return UserToken
     */
    public function find(string $id) : ?UserToken
    {
        return $this->findOneByAttributes(['id' => $id]);
    }

    /**
     * @param string $token
     * @param string $type
     * @return UserToken|null
     */
    public function findByTokenAndType(string $token, string $type) : ?UserToken
    {
        return $this->findOneByAttributes(['token' =>$token, 'type' => $type]);
    }

    /**
     * @param string $token
     * @param string $type
     * @return bool
     */
    public function existsByTokenAndType(string $token, string $type) : bool
    {
        $query = new Query();
        return $query->from(YiiSqlUserTokenRepository::TABLE_NAME)
            ->where(['token' => $token, 'type' => $type])
            ->exists();
    }

    /**
     * @param $params
     * @return UserToken|null
     */
    private function findOneByAttributes($params) : ?UserToken
    {
        $query = new Query();
        $row = $query->from(YiiSqlUserTokenRepository::TABLE_NAME)
            ->andWhere($params)
            ->one();
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param $result
     * @return UserToken
     */
    private function toEntity(array $result) : UserToken
    {
        return $this->userTokenHydrator->hydrate($result);
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id) : bool
    {
        $query = new Query();
        return $query->from(YiiSqlUserTokenRepository::TABLE_NAME)
            ->where(['id' => $id])
            ->exists();
    }
}