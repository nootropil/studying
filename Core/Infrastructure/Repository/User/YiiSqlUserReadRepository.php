<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Repository\User;

use app\Core\Domain\Model\User\User;
use app\Core\Domain\Repository\User\UserReadRepository;
use yii\db\Query;

final class YiiSqlUserReadRepository implements UserReadRepository
{
    /**
     * @var UserHydrator
     */
    private $userHydrator;

    /**
     * YiiSqlUserReadRepository constructor.
     */
    public function __construct()
    {
        $this->userHydrator = new UserHydrator();
    }

    /**
     * @param $id
     * @return User
     */
    public function find(string $id) : ?User
    {
        return $this->findOneByAttribute('id', $id);
    }

    /**
     * @param $username
     * @return User|null
     */
    public function findByUsername(string $username) : ?User
    {
        return $this->findOneByAttribute('LOWER(username)', mb_strtolower($username));
    }

    /**
     * @return User[]
     */
    public function findAll() : array
    {
        $query = new Query();
        $rows = $query->from(YiiSqlUserRepository::TABLE_NAME)
            ->orderBy(['username' => SORT_ASC])->all();
        $users = [];
        foreach ($rows as $row) {
            array_push($users, $this->toEntity($row));
        }
        return $users;
    }

    /**
     * @param $attribute
     * @param $value
     * @return User|null
     */
    private function findOneByAttribute(string $attribute, $value) : ?User
    {
        $query = new Query();
        $row = $query->from(YiiSqlUserRepository::TABLE_NAME)
            ->andWhere([$attribute => $value])
            ->one();
        if ($row) {
            return $this->toEntity($row);
        } else {
            return null;
        }
    }

    /**
     * @param $result
     * @return User
     */
    private function toEntity(array $result) : User
    {
        return $this->userHydrator->hydrate($result);
    }

    /**
     * @param string $email
     * @return User|null
     */
    public function findByEmail(string $email) : ?User
    {
        return $this->findOneByAttribute('LOWER(email)', mb_strtolower($email));
    }

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id) : bool
    {
        $query = new Query();
        return $query->from(YiiSqlUserRepository::TABLE_NAME)
            ->where(['id' => $id])
            ->exists();
    }

    /**
     * @param string $username
     * @return bool
     */
    public function existsByUsername(string $username) : bool
    {
        $query = new Query();
        return $query->from(YiiSqlUserRepository::TABLE_NAME)
            ->where(['LOWER(username)' => mb_strtolower($username)])
            ->exists();
    }

    /**
     * @param string $email
     * @return bool
     */
    public function existsByEmail(string $email) : bool
    {
        $query = new Query();
        return $query->from(YiiSqlUserRepository::TABLE_NAME)
            ->where(['LOWER(email)' => mb_strtolower($email)])
            ->exists();
    }

    /**
     * @param array $entities
     * @return array
     */
    public function toIdAndNameArray(array $entities) : array
    {
        /** @var $entity User*/
        $array = [];
        foreach($entities as $entity) {
            $array[$entity->getId()] = $entity->getUsername();
        }
        return $array;
    }
}