<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Repository\User;

use app\Core\Domain\Model\User\User;
use app\Core\Domain\Repository\User\UserRepository;
use app\Core\Infrastructure\Service\IdentityGenerator\Uuid4Generator;
use yii\db\Query;

final class YiiSqlUserRepository implements UserRepository
{
    const TABLE_NAME = 'user';

    /**
     * @var UserHydrator
     */
    private $userHydrator;

    /**
     * YiiSqlUserReadRepository constructor.
     */
    public function __construct()
    {
        $this->userHydrator = new UserHydrator();
    }

    /**
     * @param User $user
     */
    public function add(User $user) : void
    {
        $columns = $this->userHydrator->extract($user);
        $columns['id'] = $user->getId();
        $query = new Query();
        $query->createCommand()->insert(self::TABLE_NAME, $columns)->execute();
    }

    /**
     * @param User $user
     */
    public function save(User $user) : void
    {
        $columns = $this->userHydrator->extract($user);
        $query = new Query();
        $query->createCommand()->update(self::TABLE_NAME, $columns, ['id' => $user->getId()])->execute();
    }

    /**
     * @param User $user
     */
    public function remove(User $user) : void
    {
        $query = new Query();
        $query->createCommand()->delete(self::TABLE_NAME, ['id' => $user->getId()])->execute();
    }

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity() : string
    {
        $uuidGenerator = new Uuid4Generator();
        return $uuidGenerator->generate();
    }  
}