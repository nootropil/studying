<?php
declare(strict_types = 1);

namespace app\Core\Infrastructure\Repository\User;

use app\Core\Domain\Model\User\User;
use app\Core\Infrastructure\Repository\Hydrator;

final class UserHydrator implements Hydrator
{
    /**
     * @param array $columns
     * @return User
     */
    public function hydrate(array $columns) : User
    {
        return User::createFromStorage(
            $columns['id'],
            $columns['username'],
            $columns['email'],
            $columns['password_hash'],
            $columns['role'],
            $columns['status'],
            $columns['created'],
            $columns['updated']
        );
    }

    /**
     * @param $entity
     * @return array
     */
    public function extract($entity) : array 
    {
        /* @var $entity User */
        $columns = [
            'username' => $entity->getUsername(),
            'email' => $entity->getEmail(),
            'password_hash' => $entity->getPasswordHash(),
            'role' => $entity->getRole(),
            'status' => $entity->getStatus(),
            'created' => $entity->getCreated()->format('Y-m-d H:i:s'),
            'updated' => $entity->getUpdated() !== null ? $entity->getUpdated()->format('Y-m-d H:i:s') : null
        ];
        return $columns;
    }
}