<?php
declare(strict_types = 1);

namespace app\Core\Domain\Service\String;

interface RandomStringGenerator
{
    /**
     * @param int $length
     * @return string
     */
    public function generate(?int $length) : string;

}