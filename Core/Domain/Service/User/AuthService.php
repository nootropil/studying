<?php
declare(strict_types = 1);

namespace app\Core\Domain\Service\User;

interface AuthService
{
    /**
     * @param $login
     * @param $password
     * @return bool
     */
    public function login(string $login, string $password) : bool;

    /**
     * @return string
     */
    public function getErrorMessage() : string;
}