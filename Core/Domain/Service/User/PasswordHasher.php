<?php
declare(strict_types = 1);

namespace app\Core\Domain\Service\User;

interface PasswordHasher
{
    /**
     * @param string $password
     * @return string
     */
    public function hash(string $password) : string;
}
