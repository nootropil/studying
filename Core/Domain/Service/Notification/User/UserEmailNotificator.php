<?php
declare(strict_types = 1);

namespace app\Core\Domain\Service\Notification\User;

use app\Core\Domain\Model\User\User;
use app\Core\Domain\Model\UserToken\UserToken;

interface UserEmailNotificator
{
    /**
     * @param UserToken $token
     * @param User $user
     */
    public function sendEmailConformationEmail(UserToken $token, User $user) : void;

    /**
     * @param UserToken $token
     * @param User $user
     */
    public function sendPasswordRecoveringEmail(UserToken $token, User $user) : void;
}