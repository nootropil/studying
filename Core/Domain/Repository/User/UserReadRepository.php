<?php
declare(strict_types = 1);

namespace app\Core\Domain\Repository\User;

use app\Core\Domain\Model\User\User;

interface UserReadRepository
{
    /**
     * @param $id
     * @return User|null
     */
    public function find(string $id) : ?User;

    /**
     * @return User[]
     */
    public function findAll() : array;

    /**
     * @param $email
     * @return User|null
     */
    public function findByEmail(string $email) : ?User;

    /**
     * @param $username
     * @return User|null
     */
    public function findByUsername(string $username) : ?User;

    /**
     * @param string $username
     * @return bool
     */
    public function existsByUsername(string $username) : bool;

    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id) : bool;

    /**
     * @param string $email
     * @return bool
     */
    public function existsByEmail(string $email) : bool;

    /**
     * @param array $entities
     * @return array
     */
    public function toIdAndNameArray(array $entities) : array;
}