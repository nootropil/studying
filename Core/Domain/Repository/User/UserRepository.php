<?php
declare(strict_types = 1);

namespace app\Core\Domain\Repository\User;

use app\Core\Domain\Model\User\User;

interface UserRepository
{
    /**
     * @param User $user
     */
    public function add(User $user) : void;

    /**
     * @param User $user
     */
    public function save(User $user) : void;

    /**
     * @param User $user
     */
    public function remove(User $user) : void;

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity() : string;
}