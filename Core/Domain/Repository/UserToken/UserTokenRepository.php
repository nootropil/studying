<?php
declare(strict_types = 1);

namespace app\Core\Domain\Repository\UserToken;

use app\Core\Domain\Model\UserToken\UserToken;

interface UserTokenRepository
{
    /**
     * @param UserToken $userToken
     */
    public function add(UserToken $userToken) : void;

    /**
     * @param UserToken $userToken
     */
    public function save(UserToken $userToken) : void;

    /**
     * @param UserToken $userToken
     */
    public function remove(UserToken $userToken) : void;

    /**
     * Get next id
     *
     * @return string
     */
    public function nextIdentity() : string;
}