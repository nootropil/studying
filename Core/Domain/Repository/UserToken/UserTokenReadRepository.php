<?php
declare(strict_types = 1);

namespace app\Core\Domain\Repository\UserToken;

use app\Core\Domain\Model\UserToken\UserToken;

interface UserTokenReadRepository
{
    /**
     * @param $id
     * @return UserToken|null
     */
    public function find(string $id) : ?UserToken;

    /**
     * @param string $token
     * @param string $type
     * @return UserToken|null
     */
    public function findByTokenAndType(string $token, string $type) : ?UserToken;

    /**
     * @param string $token
     * @param string $type
     * @return bool
     */
    public function existsByTokenAndType(string $token, string $type) : bool;



    /**
     * @param string $id
     * @return bool
     */
    public function exists(string $id) : bool;

}