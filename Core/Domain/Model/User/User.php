<?php
declare(strict_types = 1);

namespace app\Core\Domain\Model\User;

use app\Core\Domain\Contract\HasId;
use DateTime;

final class User implements HasId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $passwordHash;

    /**
     * @var string
     */
    private $role;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime|null
     */
    private $updated;

    /**
     * User constructor.
     * @param string $id
     * @param string $username
     * @param string $email
     * @param string $passwordHash
     * @param string $role
     * @param string $status
     * @param DateTime $created
     * @param DateTime|null $updated
     */
    private function __construct(
        string $id,
        string $username,
        string $email,
        string $passwordHash,
        string $role,
        string $status,
        DateTime $created,
        ?DateTime $updated
    )
    {
        $this->id = $id;
        $this->username = $username;
        $this->passwordHash = $passwordHash;
        $this->email = $email;
        $this->role = $role;
        $this->status = $status;
        $this->created = $created;
        $this->updated = $updated;
    }

    /**
     * @param string $id
     * @param string $username
     * @param string $email
     * @param string $passwordHash
     * @param string $role
     * @param string $status
     * @param string $created
     * @param null|string $updated
     * @return User
     */
    public static function createFromStorage(
        string $id,
        string $username,
        string $email,
        string $passwordHash,
        string $role,
        string $status,
        string $created,
        ?string $updated
    ) : User
    {
        $updated = $updated !== null ? new DateTime($updated) : null;
        $user = new User(
            $id,
            $username,
            $email,
            $passwordHash,
            $role,
            $status,
            new DateTime($created),
            $updated
        );
        return $user;
    }

    /**
     * @param string $id
     * @param string $username
     * @param string $email
     * @param string $passwordHash
     * @return User
     */
    public static function registerNew(
        string $id,
        string $username,
        string $email,
        string $passwordHash
    ) : User
    {
        $user = new User(
            $id,
            $username,
            $email,
            $passwordHash,
            Roles::UNCONFIRMED_USER,
            Statuses::ACTIVE,
            new DateTime('now'),
            null
        );
        return $user;
    }

    /**
     * @param string $username
     * @param string $email
     * @param string $role
     */
    public function changeUserData(
        string $username,
        string $email,
        string $role
    ) : void
    {
        $this->username = $username;
        $this->email = $email;
        $this->role = $role;
    }

    public function setRoleUser() : void
    {
        $this->role = Roles::USER;
    }

    /**
     * @param string $passwordHash
     */
    public function changePasswordHash(string $passwordHash) : void
    {
        $this->passwordHash = $passwordHash;
    }

    /**
     * @param string $status
     */
    public function changeStatus(string $status) : void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->status == Statuses::ACTIVE;
    }

    /**
     * @return bool
     */
    public function isBlocked() : bool
    {
        return $this->status == Statuses::BLOCKED;
    }

    public function defineUpdated()
    {
        $this->updated = new \DateTime('now');
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @return mixed
     */
    public function getPasswordHash() : string
    {
        return $this->passwordHash;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getRole() : string
    {
        return $this->role;
    }

    /**
     * @return string
     */
    public function getStatus() : string
    {
        return $this->status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : DateTime
    {
        return $this->created;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdated() : ?DateTime
    {
        return $this->updated;
    }

    /**
     * @return bool
     */
    public function isAdmin() : bool
    {
        return $this->role === Roles::ADMIN;
    }

    /**
     * @return bool
     */
    public function isModerator() : bool
    {
        return $this->role === Roles::USER;
    }

    /**
     * @return bool
     */
    public function isUser() : bool
    {
        return $this->role === Roles::USER;
    }

    /**
     * @return bool
     */
    public function isUnconfirmedUser() : bool
    {
        return $this->role === Roles::UNCONFIRMED_USER;
    }
}