<?php
declare(strict_types = 1);

namespace app\Core\Domain\Model\User;

final class Statuses
{
    const ACTIVE = 'active';
    const BLOCKED = 'blocked';
}