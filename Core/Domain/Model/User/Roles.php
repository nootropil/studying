<?php
declare(strict_types = 1);

namespace app\Core\Domain\Model\User;

final class Roles
{
    const ADMIN = 'admin';
    const MODERATOR = 'moderator';
    const USER = 'user';
    const UNCONFIRMED_USER = 'unconfirmed-user';


}