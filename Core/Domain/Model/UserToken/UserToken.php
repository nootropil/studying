<?php
declare(strict_types = 1);

namespace app\Core\Domain\Model\UserToken;

use app\Core\Domain\Contract\HasId;
use DateTime;

final class UserToken implements HasId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $userId;
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $type;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * UserTokenToken constructor.
     * @param string $id
     * @param string $userId
     * @param string $token
     * @param string $type
     * @param DateTime $created
     */
    private function __construct(
        string $id,
        string $userId,
        string $token,
        string $type,
        DateTime $created
    )
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->type = $type;
        $this->token = $token;
        $this->created = $created;
    }

    /**
     * @param string $id
     * @param string $userId
     * @param string $token
     * @param string $type
     * @param string $created
     * @return UserToken
     */
    public static function createFromStorage(
        string $id,
        string $userId,
        string $token,
        string $type,
        string $created
    ) : UserToken
    {
        $userToken = new UserToken(
            $id,
            $userId,
            $token,
            $type,
            new DateTime($created)
        );
        return $userToken;
    }

    /**
     * @param string $id
     * @param string $userId
     * @param string $token
     * @param string $type
     * @return UserToken
     */
    public static function createNew(
        string $id,
        string $userId,
        string $token,
        string $type
    ) : UserToken
    {
        $userToken = new UserToken(
         $id,
         $userId,
         $token,
         $type,
            new DateTime('now')
        );
        return $userToken;
    }

    /**
     * @return string
     */
    public function getId() : string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUserId() : string
    {
        return $this->userId;
    }


    /**
     * @return string
     */
    public function getToken() : string
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getType() : string
    {
        return $this->type;
    }

    /**
     * @return \DateTime
     */
    public function getCreated() : DateTime
    {
        return $this->created;
    }
}