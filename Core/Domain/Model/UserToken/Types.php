<?php
declare(strict_types = 1);

namespace app\Core\Domain\Model\UserToken;

final class Types
{
    const EMAIL_CONFIRMATION = 'email-confirmation';
    const PASSWORD_RECOVERING = 'password-recovering';
}