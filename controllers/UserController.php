<?php
namespace app\controllers;

use app\Core\Application\{
    Command\User\RecoverUserPassword,
    Command\User\ConfirmUserEmail,
    Command\User\RegisterUser, Command\CommandBus,
    Event\AsyncEventBus,
    Event\User\EmailConformationRequested,
    Event\User\PasswordRecoveringRequested,
    Validator\UserToken\EmailConfirmationValidator,
    Validator\UserToken\PasswordRecoveringValidator
};
use app\Forms\User\RegisterUserForm;
use app\Forms\User\{
    EmailConformationRequestForm, PasswordRecoveringForm, PasswordRecoveringRequestFrom
};
use app\Helpers\View\Config\Constants;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;

class UserController extends Controller
{
    public $layout = Constants::LAYOUT_MAIN;

    /**
     * @var CommandBus
     */
    private $commandBus;

    /**
     * @var EmailConfirmationValidator
     */
    private $emailConfirmationValidator;

    /**
     * @var PasswordRecoveringValidator
     */
    private $passwordRecoveringValidator;

    /**
     * @var AsyncEventBus
     */
    private $asyncEventBus;

    /**
     * UserController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param array $config
     * @param AsyncEventBus $asyncEventBus
     * @param CommandBus $commandBus
     * @param EmailConfirmationValidator $emailConfirmationValidator
     * @param PasswordRecoveringValidator $passwordRecoveringValidator
     */
    public function __construct(
        $id,
        $module,
        $config = [],
        AsyncEventBus $asyncEventBus,
        CommandBus $commandBus,
        EmailConfirmationValidator $emailConfirmationValidator,
        PasswordRecoveringValidator $passwordRecoveringValidator
    ) {
        $this->asyncEventBus = $asyncEventBus;
        $this->commandBus = $commandBus;
        $this->passwordRecoveringValidator = $passwordRecoveringValidator;
        $this->emailConfirmationValidator = $emailConfirmationValidator;
        parent::__construct($id, $module, $config);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                    ],
                ],
            ]
        ];
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'maxLength' => 3,
                'minLength' => 3
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionRegistration() {
        /* @var $model RegisterUserForm */
        $model = Yii::createObject(RegisterUserForm::className());
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->commandBus->handle(new RegisterUser($model->getPreparedArray()));
            Yii::$app->getSession()->setFlash('success', Yii::t('main', 'REGISTERED'));
            return $this->redirect(['/default/message']);
        }
        return $this->render('registration', [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     * @return \yii\web\Response
     */
    public function actionEmailConfirmation($token)
    {
        $validator = $this->emailConfirmationValidator->validate($token);
        if ($validator->isValid()) {
            $this->commandBus->handle(new ConfirmUserEmail($token));
            Yii::$app->getSession()->setFlash('success', Yii::t('main', 'EMAIL_CONFIRMED'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('main', $validator->getError()));
        }
        return $this->redirect(['/default/message']);
    }

    /**
     *
     */
    public function actionEmailConfirmationRequest()
    {
        /* @var $model EmailConformationRequestForm */
        $model = Yii::createObject(EmailConformationRequestForm::className());
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->asyncEventBus->handle(new EmailConformationRequested($model->email));
            Yii::$app->getSession()->setFlash('success', Yii::t('main', 'EMAIL_CONFIRMATION_REQUESTED'));
            return $this->redirect(['/default/message']);
        }
        return $this->render('email_confirmation_request', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionPasswordRecoveringRequest()
    {
        /* @var $model PasswordRecoveringRequestFrom */
        $model = Yii::createObject(PasswordRecoveringRequestFrom::className());
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $this->asyncEventBus->handle(new PasswordRecoveringRequested($model->email));
            Yii::$app->getSession()->setFlash('success', Yii::t('main', 'PASSWORD_RECOVERING_REQUESTED'));
            return $this->redirect(['/default/message']);
        }
        return $this->render('password_recovering_request', [
            'model' => $model,
        ]);
    }

    /**
     * @param $token
     * @return string|\yii\web\Response
     */
    public function actionPasswordRecovering($token)
    {
        $validator = $this->passwordRecoveringValidator->validate($token);
        if ($validator->isValid()) {
            $model = Yii::createObject(PasswordRecoveringForm::className());
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $this->commandBus->handle(new RecoverUserPassword($token, $model->password));
                Yii::$app->getSession()->setFlash('success', Yii::t('main', 'PASSWORD_RECOVERED'));
                return $this->redirect(['/default/message']);
            }
            return $this->render('password_recovering', [
                'model' => $model
            ]);
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('main', $validator->getError()));
        }
        return $this->redirect(['/default/message']);
    }
}
